class popMonster extends Thread
{
  int delay;
  int i;
  int col;
  
  popMonster(int delay,int col)
  {
    this.delay = delay;
    i = 0;
    start();
    this.col = col;
  }
  
  void run()
  {
    ArrayList<Integer> allocateByLevelCopy = new ArrayList();
    allocateByLevelCopy.addAll(allocateByLevel);
    while(i<10)
    {
      try
      {
        Thread.sleep(300+200*delay);
      }  catch(Exception e){}
      
      int index = int(random(allocateByLevelCopy.size()));
      showedMonsters.add(new monster(800,col,allocateByLevelCopy.get(index)));
      allocateByLevelCopy.remove(index);
      i++;
      
      try
      {
        Thread.sleep(appearanceDelay); // depend on gamelevel
      }  catch(Exception e){}
      
      if(i == 9) end.add(true);     
    }  
  }
}

class popHeart extends Thread
{
  int i;
  int col;
  
  popHeart(int col)
  {
    i = 0;
    start();
    this.col = col;
  }
  
  void run()
  {
    while(i<3)
    {
      try
      {
        Thread.sleep(10000);   //depend on gamelevel
      }  catch(Exception e){}
      
      showedHearts.add(new heart(800,col));
      i++;
      
      try
      {
        Thread.sleep(10000);
      }  catch(Exception e){}
    
    }  
  }
}



abstract class target
{
  String name;
  PImage picture;
  float x,y;
  
  target(int row, int col)
  {   
    
    this.x = row;
    this.y = 100 + col*150;
  }
  
  abstract void namelist();
  
  boolean checking(String typing)
  {
    if(typing.equals(name))
      return true;
    else return false;
  }
  
  abstract void draw();
}

class monster extends target
{
  int level;
  
  monster(int row, int col, int level)
  {
    super(row, col);
    this.level = level;
    namelist();
  }
  
  void namelist()
  {
    if(level == 1)
    {
      String[] wordbank = loadStrings("wordbank.txt");
      int index = int(random(wordbank.length));
      name = wordbank[index];
    }
    if(level == 2)
    {
      String[] wordbank = loadStrings("wordbank_lv2.txt");
      int index = int(random(wordbank.length));
      name = wordbank[index];
    }
    if(level == 3)
    {
      String[] wordbank = loadStrings("wordbank_lv3.txt");
      int index = int(random(wordbank.length));
      name = wordbank[index];
    }
  }
  
  void draw()
  {
    if(level == 1)
    {
      picture = loadImage("lv1.png");
      image(picture,x,y,112,102);
      x-=0.5;
    }
    if(level == 2)
    {
      picture = loadImage("lv2.png");
      image(picture,x,y-48,112,150);
      x-=0.7;
    }
    if(level == 3)
    {
      picture = loadImage("lv3.png");
      image(picture,x,y-49,112,151);
      x-=0.9;
    }
    
    if(textWidth(name)>110) textSize(16);
    else textSize(18);
    textAlign(CENTER);
    fill(0);
    text(name,x+56,y+97);
  }
}

class heart extends target
{ 
  heart(int row, int col)
  {
    super(row, col);
    namelist();
  }
  
  void namelist()
  {
   String[] wordbank = loadStrings("wordbank_heart.txt");
   int index = int(random(wordbank.length));
   name = wordbank[index];
  }
  
  void draw()
  {
    picture = loadImage("heart.png");
    image(picture,x,y,112,102);
    x-=0.5;
    
    if(textWidth(name)>110) textSize(16);
    else textSize(18);
    textAlign(CENTER);
    fill(0);
    text(name,x+56,y+97);
  }
}
